# Release information about TGLightWindow

version = "2.0"

description = "LightWindow JS packaged as TurboGears widget"
# long_description = "More description about your plan"
author = "W-Mark Kubacki"
email = "wmark.tglightwindow@hurrikane.de"
copyright = "Author; LightWindow: Copyright (c) 2007, Stickmanlabs (Kevin Miller et al)"

# if it's open source, you might want to specify these
url = "http://tgwidgets.ossdl.de/"
download_url = "http://static.ossdl.de/tgwidgets/downloads/"
license = "MIT"
